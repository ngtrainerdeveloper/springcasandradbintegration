package com.rdutta.cassandra.test.SpringCassandraProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCassandraProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCassandraProjectApplication.class, args);
	}

}
